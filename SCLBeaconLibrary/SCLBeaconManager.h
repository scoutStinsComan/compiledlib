//
//  SCLBeaconManager.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 29.02.16.
//  Copyright © 2016 Stins Coman. All rights reserved.
//

#import "SCLLibrary.h"

@protocol SCLLibraryDelegate;

@interface SCLBeaconManager : NSObject

@property (nonatomic, strong) id<SCLLibraryDelegate>delegate;
@property (nonatomic, assign) BOOL paused;

+ (instancetype)sharedManager;

- (void)startMonitoring;
- (void)dataByRuleKey:(NSString *)ruleKey;

@end
