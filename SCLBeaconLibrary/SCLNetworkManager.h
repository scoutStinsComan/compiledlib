//
//  SCLNetworkManager.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 15.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIImage;
@interface SCLNetworkManager : NSObject

+ (instancetype)sharedInstance;

- (void)downloadDataWithSuccess:(void(^)(void))success andFailure:(void(^)(NSError *error))failure;
- (void)downloadImageDataWithID:(int)ID success:(void (^)(UIImage *image))succsess andFailure:(void(^)(NSError *error))failure;

@end
