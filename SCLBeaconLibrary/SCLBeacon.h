//
//  SCLBeacon.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 14.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLBeacon : NSObject

@property (nonatomic, assign) int ID;
@property (nonatomic, assign) int major;
@property (nonatomic, assign) int minor;
@property (nonatomic, assign) double accuracy;
@property (nonatomic, assign) BOOL is_new;
@property (nonatomic, assign) double coordX;
@property (nonatomic, assign) double coordY;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int location_id;
@property (nonatomic, strong) NSUUID *uuid;
@property (nonatomic, assign) double distance_coeff;
@property (nonatomic, assign) int default_content_id;

+ (SCLBeacon*)initWithDictionary: (NSDictionary*) dictionary;

@end
