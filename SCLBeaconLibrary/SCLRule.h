//
//  SCLRule.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 20.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLRule : NSObject

@property (nonatomic, assign) int ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) int location_id;
@property (nonatomic, assign) int parent_id;
@property (nonatomic, assign) int push_message_id;
@property (nonatomic, assign) int content_template_id;

+ (SCLRule*)initWithDictionary:(NSDictionary *)dictionary;

@end
