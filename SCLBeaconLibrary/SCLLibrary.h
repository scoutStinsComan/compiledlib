//
//  SCLLibrary.h
//  SCBeaconLib
//
//  Created by Igor Aleshin on 28.02.16.
//  Copyright © 2016 Stins Coman. All rights reserved.
//


@import Foundation;
@import UIKit;
@import CoreLocation;

#import "SCLBeaconManager.h"
#import "SCLNetworkManager.h"
#import "SCLRecognizer.h"
#import "SCLBeacon.h"
#import "SCLContentTemplate.h"
#import "SCLLocation.h"
#import "SCLPush.h"
#import "SCLRule.h"
#import "SCLVault.h"

typedef NS_ENUM(NSUInteger, SCLRangingDelay) {
    SCLZeroDelay, //Recomended for testing only
    SCLFifteenMinDelay, //Ranging delay for 15 min
    SCLHalfHourDelay, //Ranging delay for 30 min
    SCLHourDelay, //Ranging delay for an hour
    SCLDayDelay //Ranging delay for a day
};

@protocol SCLLibraryImageDelegate <NSObject>

/* Called when downloading data get error. Method (UIImage *)downoladImageByID:(int)ID will return nil */
- (void)imageDownloadDidFail:(NSError *)error;

@end

@protocol SCLLibraryDelegate <NSObject>

@required
/*Called when beacon of any region has reached */
- (void)userDidEnterLocation:(int)ID name:(NSString *)name inTime:(NSDate *)now;
/* Called when downloading data from CMS server get error. If calls at first start, beacons ranging won't start */
- (void)dataDownloadDidFail:(NSError *)error;

@optional

- (void)locationsPush:(int)ID Title:(NSString *)name andText:(NSString *)text;
- (void)userDidEnterRegion:(NSDate *)now;
- (void)locationsContent:(SCLContentTemplate *)content;
- (void)userReachBeaconWithCoordinate:(double)x :(double)y andAccuracy:(double)accuracy;

@end

@interface SCLLibrary : NSObject

@property (nonatomic, strong) NSString *authKey; //Need to set before starting library
@property (nonatomic, strong) id<SCLLibraryDelegate>delegate; //Set to get beacons content
@property (nonatomic, strong) id<SCLLibraryImageDelegate>imageDelegate; //Set to handle image download error;
@property (nonatomic, assign) BOOL canUpdateData; //Can change while library working
@property (nonatomic, assign) SCLRangingDelay rangingDelay; //Can change while library working. Recomended to set something rather than SCLZeroDelay before start library

+ (instancetype)sharedLibrary;

- (void)start;
- (UIImage *)downloadImageByID:(int)ID;
- (void)dataByRuleKey:(NSString *)ruleKey; //recomended to call mathod in last line of - (void)userDidEnterRegion:(NSDate *)now;
- (void)setPause:(BOOL)pause; //recomended set ranging to pause while other actions must do


@end
