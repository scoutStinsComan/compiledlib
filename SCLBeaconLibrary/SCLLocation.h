//
//  SCLLocation.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 17.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

@import Foundation;

@interface SCLLocation : NSObject

@property (assign, nonatomic) int ID;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) float radius;
@property (assign, nonatomic) int major;
@property (assign, nonatomic) int minor;
@property (strong, nonatomic) NSUUID *uuid;
@property (strong, nonatomic) NSDictionary *rules;
@property (strong, nonatomic) NSArray *beacons;


+ (SCLLocation*)initWithDictionary:(NSDictionary*) dictionary;

@end
