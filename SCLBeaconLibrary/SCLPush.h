//
//  SCLPush.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 20.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLPush : NSObject

@property (nonatomic, assign) int ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *text;

+ (SCLPush*)initWithDictionary:(NSDictionary *)dictionary;

@end
