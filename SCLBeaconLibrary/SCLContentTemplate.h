//
//  SCLContentTemplate.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 20.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLContentTemplate : NSObject

@property (nonatomic, assign) int ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *button_title;
@property (nonatomic, strong) NSURL *button_url;
@property (nonatomic, assign) int image_id;

+ (SCLContentTemplate*)initWithDictionary:(NSDictionary*)dictionary;

@end
