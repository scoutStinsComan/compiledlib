//
//  SCLVault.h
//  SCBeaconLib
//
//  Created by Igor Aleshin on 28.02.16.
//  Copyright © 2016 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SCLBeacon;
@class SCLLocation;
@class SCLContentTemplate;
@class SCLRule;
@class SCLPush;
@class CLBeacon;

@interface SCLVault : NSObject

@property (strong, nonatomic) NSDictionary *masterCount;
@property (assign, nonatomic) BOOL vaultReady;

+ (instancetype)sharedVault;


//Save
- (void)saveLocation:(SCLLocation *)location;
- (void)saveBeacon:(SCLBeacon *)beacon;
- (void)savePush:(SCLPush *)push;
- (void)saveContent:(SCLContentTemplate *)content;
- (void)saveRule:(SCLRule *)rule;

//Load
- (NSArray *)loadAllLocations;
- (SCLLocation *)loadLocationByBeacon:(CLBeacon *)beacon;
- (SCLRule *)loadRule:(int)ID;
- (SCLPush *)loadPush:(int)ID;
- (SCLContentTemplate *)loadContent:(int)ID;
- (BOOL)isBeaconInVault:(CLBeacon *)beacon;


@end
