//
//  SCLRecognizer.h
//  SCBeaconLib
//
//  Created by Алешин Игорь on 16.07.15.
//  Copyright (c) 2015 Stins Coman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLRecognizer : NSObject

- (void)recognizeResponse:(id)response;

@end
